import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import ModalTarea from './ModalTarea';
import SearchBar from '../Components/SearchBar';
import Check from '../Components/Check';
import './Styles.css';

function CrearTarea() {
	const getData = () => {
		const titulos = JSON.parse(localStorage.getItem('titulos'));
		return titulos || [];
	};

	const [titulos, setTitulos] = useState(getData());
	const [contadorTareas, setContadorTareas] = useState(titulos.length);
	const [contadorTareasCompletas, setContadorTareasCompletas] = useState(0);
	const [searchTerm, setSearchTerm] = useState('');
	const [modalIsOpen, setModalIsOpen] = useState(false);

	const handleTareaCompletada = (index) => {
		const tareaActualizada = titulos[index];
		tareaActualizada.completada = !tareaActualizada.completada;

		const titulosActualizados = [...titulos];
		titulosActualizados[index] = tareaActualizada;

		setTitulos(titulosActualizados);

		if (tareaActualizada.completada) {
			setContadorTareasCompletas((prevCount) => prevCount + 1);
		} else {
			setContadorTareasCompletas((prevCount) => Math.max(prevCount - 1, 0));
		}
	};
	const handleTareaAdded = () => {
		const titulosActualizados = getData();
		setContadorTareas(titulosActualizados.length);
		setTitulos(titulosActualizados);
		closeModal();
	};

	const openModal = () => {
		setModalIsOpen(true);
	};

	const closeModal = () => {
		setModalIsOpen(false);
	};

	const handleClick = (index) => {
		const confirmarEliminacion = window.confirm('¿Realmente deseas eliminar la tarea?');
		if (confirmarEliminacion) {
			const titulosActualizados = [...titulos];
			titulosActualizados.splice(index, 1);
			setTitulos(titulosActualizados);
			setContadorTareas(titulosActualizados.length);
			localStorage.setItem('titulos', JSON.stringify(titulosActualizados));
				const tareasCompletas = titulosActualizados.filter(
					(tarea) => tarea.completada
				);
			setContadorTareasCompletas(tareasCompletas.length);
			alert('Tarea eliminada exitosamente');
		}
		else {
			alert('La tarea no fue eliminada.');
		}	
	};

	useEffect(() => {
		setTitulos(getData());
	}, []);

	const handleSearch = (query) => {
		setSearchTerm(query);
		const filteredTasks = getData().filter(
			(task) => task.titulo.includes(query) || task.descripcion.includes(query)
		);
		setTitulos(filteredTasks);
	};
	const containerStyle = {
		backgroundColor: '#F5EFE6',
		height: '100vh',

		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
	};

	return (
		<div className='container' style={containerStyle}>
			<h2
				style={{
					marginBottom: '20px',
					fontSize: '60px',
					color: '#333',
					marginTop: -20,
					fontWeight: 'bold',
				}}
			>
				App de Notas
			</h2>
			<SearchBar onSearch={handleSearch} value={searchTerm} />

			<button
				style={{
					width: 200,
					height: 45,
					borderRadius: 20,
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: '#C7E7F5',
					color: '#C7E7F5',
					borderColor: '#C7E7F5',
					marginTop: 20,
				}}
				onClick={openModal}
			>
				<text style={{ color: '#000', fontSize: 18, fontWeight: 'bold' }}>
					{' '}
					Nueva Tarea
				</text>
			</button>
			<Modal
				isOpen={modalIsOpen}
				onRequestClose={closeModal}
				contentLabel='Crear Tarea Modal'
				style={{
					content: {
						width: '50%',
						maxHeight: '50%',
						margin: 'auto',
					},
				}}
			>
				<ModalTarea closeModal={closeModal} onTareaAdded={handleTareaAdded} />
			</Modal>
			<div style={{ display: 'flex', alignItems: 'center' }}>
				<h2 style={{ marginRight: '30px' }}>Tareas Guardadas:</h2>
				<div
					className='responsivo-div'
					style={{
						backgroundColor: '#247BA0',
						borderRadius: 80,
						height: 30,
						width: 250,
						textAlign: 'center',
						marginTop: 5,
						marginRight: '30px',
					}}
				>
					<p
						style={{
							color: '#fff',
							fontSize: 15,
							fontWeight: 'bold',
							marginTop: 5,
						}}
					>
						Total de tareas: {contadorTareas}
					</p>
				</div>
				<div
					className='responsivo-div'
					style={{
						backgroundColor: '#247BA0',
						borderRadius: 80,
						height: 30,
						width: 250,
						textAlign: 'center',
						marginTop: 5,
						marginRight: '30px',
					}}
				>
					<p
						style={{
							color: '#fff',
							fontSize: 15,
							fontWeight: 'bold',
							marginTop: 5,
						}}
					>
						Tareas completas: {contadorTareasCompletas}
					</p>
				</div>
			</div>

			<div style={{ overflow: 'auto', maxHeight: '350px' }}>
				{titulos.length === 0 ? (
					<p>No se encontraron resultados.</p>
				) : (
					<table
						className='responsive-table'
						style={{
							width: '1000px',
							borderCollapse: 'collapse',
							marginTop: 10,
							border: '1px solid #ccc',
						}}
					>
						<thead>
							<tr>
								<th
									style={{
										backgroundColor: '#247BA0',
										color: '#fff',
										padding: '10px',
									}}
								>
									#
								</th>
								<th
									style={{
										backgroundColor: '#247BA0',
										color: '#fff',
										padding: '10px',
									}}
								>
									TITULO
								</th>
								<th
									style={{
										backgroundColor: '#247BA0',
										color: '#fff',
										padding: '10px',
									}}
								>
									DESCRIPCION
								</th>
								<th
									style={{
										backgroundColor: '#247BA0',
										color: '#fff',
										padding: '10px',
									}}
								>
									ACCIÓN
								</th>
							</tr>
						</thead>
						<tbody>
							{titulos.map((titulo, index) => (
								<tr key={index}>
									<td
										style={{
											backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
											padding: '10px',
										}}
									>
										{index + 1}
									</td>
									<td
										style={{
											backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
											padding: '10px',
										}}
									>
										{titulo.titulo}
									</td>
									<td
										style={{
											backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
											padding: '10px',
										}}
									>
										{titulo.descripcion}
									</td>
									<td
										style={{
											backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
											padding: '10px',
											textAlign: 'center',
											display: 'flex',
											alignItems: 'center',
										}}
									>
										<button className='eliminar-button' onClick={handleClick}>
											-
										</button>
										<span style={{ margin: '0 5px' }}></span>
										<Check
											index={index}
											tarea={titulo}
											onTareaCompletada={handleTareaCompletada}
										/>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				)}
			</div>
		</div>
	);
}

export default CrearTarea;
