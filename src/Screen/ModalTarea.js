import React, { useState, useEffect } from 'react';
import './Modal.css';
function ModalTarea({ closeModal, onTareaAdded }) {
	const [nombreTarea, setNombreTarea] = useState('');
	const [descripcionTarea, setDescripcionTarea] = useState('');
	const [titulosGuardados, setTitulosGuardados] = useState([]);

	const handleCrearTarea = () => {
		saveData();
		closeModal();
		onTareaAdded();
	};
	const saveData = () => {
		const nuevosTitulos = [
			...titulosGuardados,
			{ titulo: nombreTarea, descripcion: descripcionTarea },
		];
		setTitulosGuardados(nuevosTitulos);
		localStorage.setItem('titulos', JSON.stringify(nuevosTitulos));

		alert('Se ha guardado correctamente ');
	};
	useEffect(() => {
		const titulosGuardados = JSON.parse(localStorage.getItem('titulos')) || [];
		setTitulosGuardados(titulosGuardados);
	}, []);

	

	return (
		<div className="modal-container">
			
			<h1 className="modal-title">Crear tarea</h1>
			<div className="modal-input-container"> 
				<label className="modal-label" for='tituloTarea'>
					Título     
				</label>
				<input
					type='text'
					id='tituloTarea'
					placeholder='Titulo de la tarea'
					value={nombreTarea}
					className="modal-input"
					onChange={(e) => setNombreTarea(e.target.value)}
				/>
			</div>
			<div className="modal-input-container"> 
				<label className="modal-label" for='descripcionTarea'>
					Descripción
				</label>
				<input
					type='text'
					id='descripcionTarea'
					placeholder='Descripcion de la tarea'
					value={descripcionTarea}
					onChange={(e) => setDescripcionTarea(e.target.value)}
					className="modal-input"
				/>
			</div>
			<button
				className="modal-button"
				onClick={handleCrearTarea}
			>
				<text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>
					{' '}
					Crear Tarea
				</text>
			</button>
		</div>
	);
}
export default ModalTarea;
