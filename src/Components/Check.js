import React from 'react';
import './Check.css';
function Check({ index, tarea, onTareaCompletada }) {
  const handleCompletadoClick = () => {
    console.log('Check presionado');
    onTareaCompletada(index);
  };

  return (
    <button
      className={`check-button ${tarea.completada ? 'completada' : ''}`}
      onClick={handleCompletadoClick}
    style={{height:'55px',fontSize:'14px'}}
    >
     {tarea.completada ?'✓':'o'}
    </button>
  );
}

export default Check;
